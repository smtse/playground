package com.griffin.hkex.daily.common;

import static com.griffin.hkex.daily.common.ConfigurationManager.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;

public class Utility {
	private static Utility soleInstance;
	private XMLConfiguration config;

	private Utility() throws ConfigurationException  {
		config = new XMLConfiguration(XML_CONFIGURATION);
	}

	public static synchronized Utility getInstance() throws ConfigurationException {
		if (soleInstance == null) {
			soleInstance = new Utility();
		}
		return soleInstance;
	}

	public String getYesterday() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return sdf.format(cal.getTime());
	}

	public void loadProxyConfiguration() throws ConfigurationException {
		if ("true".equals((String)config.getProperty("http.proxy"))) {
			System.getProperties().put("http.proxyHost", config.getString(PROXY_HOST));
			System.getProperties().put("http.proxyPort", config.getString(PROXY_PORT));
		}
	}

	public String getHKEXDailyQuotationURL() throws ConfigurationException {
		return (String)config.getProperty("hkex.dayQuotUrl");
	}

	public String getHKEXDailyParseOutputDirectoy() throws ConfigurationException {
		return (String)config.getProperty("output.dailyParseDir");
	}

	public String getHKEXDailyDownloadOutputDirectoy() throws ConfigurationException {
		return (String)config.getProperty("output.dailyDownloadDir");
	}
}
