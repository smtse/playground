package com.griffin.hkex.daily.common;

public class ConfigurationManager {
	public static final String XML_CONFIGURATION = "config/config.xml";
	public static final String PROXY_HOST = "http.proxyHost";
	public static final String PROXY_PORT = "http.proxyPort";

	public static final String DAY_QUOT_URL = "hkex.dayQuotUrl";
	public static final String DAY_DOWNLOAD_DIR = "hkex.dailyDownloadDir";
	public static final String DAY_PARSE_DIR = "hkex.dailyParseDir";
}
