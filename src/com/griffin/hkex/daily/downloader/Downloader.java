package com.griffin.hkex.daily.downloader;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import org.apache.commons.configuration.ConfigurationException;

import com.griffin.hkex.daily.common.Utility;

/**
 * 
 * @author smtse
 *
 * Transaction data is available from:
 * http://www.hkex.com.hk/eng/stat/smstat/securitiesmarket.htm
 * 
 * Daily data:
 * http://www.hkex.com.hk/eng/stat/smstat/dayquot/qtn.asp
 *
 */

public class Downloader {
	public static void main(String argv[]) throws IOException, ConfigurationException {
		boolean isManualMode = argv.length == 1 ? true : false;
		Utility utility = Utility.getInstance();
		String filename = "d" + (isManualMode ? argv[1] : utility.getYesterday()) + "e.htm";
		utility.loadProxyConfiguration();

		String url = utility.getHKEXDailyQuotationURL();
		String outputDir = utility.getHKEXDailyDownloadOutputDirectoy();

		URL website = new URL(url + filename);
		ReadableByteChannel rbc = Channels.newChannel(website.openStream());
		FileOutputStream fos = new FileOutputStream(outputDir + filename);
		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		fos.close();

		System.out.println("DOWNLOAD DONE");
	}
}
