package com.griffin.hkex.daily.parser;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.commons.configuration.ConfigurationException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.griffin.hkex.daily.common.Utility;

public class Splitter {
	public static final String SECTION = "-------------------------------------------------------------------------------";

	public static void main(String[] argv) throws ConfigurationException, IOException {
		Utility utility = Utility.getInstance();
		String downloadOutputDir = utility.getHKEXDailyDownloadOutputDirectoy();
		String parseOutputDir = utility.getHKEXDailyParseOutputDirectoy();
		String filename = "d" + utility.getYesterday() + "e.htm";

		File input = new File(downloadOutputDir + filename);
		Document doc = Jsoup.parse(input, "UTF-8", "");

		String text = doc.body().text();

		String[] sections = text.split(SECTION);
		for (int i = 0; i < sections.length; i ++) {
			PrintWriter out = new PrintWriter(parseOutputDir + filename + "-" + i + ".txt");
			out.print(sections[i]);
			out.close();
		}
	}
}
