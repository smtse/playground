package com.griffin.hkex.daily.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
"\n" + 
"CODE  NAME OF STOCK    SALES RECORD\n" + 
"    1 CHEUNG KONG      < P52,000-122.627 P52,000-122.626 P7,000-122.639\n" + 
"                       110,000-122.30 >[ 18,000-122.20 2,000-122.00 \n"+ 
"                       1,000-121.30 1,000-121.40 3,000-121.30 10,000-121.40 \n"+ 
"                       13,000-121.50 2,000-121.60 ]/-//[ 3,000-121.60 \n"+ 
"                       3,000-120.70 4,000-120.40 3,000-120.70 ]\n" + 
"    2 CLP HOLDINGS     < P14,500-62.626 P500-62.75 P12,500-62.55 500-62.25\n" +  
"                       U2,000-62.25 95,000-62.25 >[\n" + 
"                       4,000-61.80 500-61.75 1,000-61.85 1,500-61.80 500-61.85\n" +  
"                       ]/-//[]\n";
 */

public class Parser {
	//private static final String pattern = "\\s(\\d*)\\s(.*)\\s\\<(.*)\\>.*";
	private static final String stockPat = ".*?\\s+(\\p{Digit}+)\\s+(.*?)\\s+<(.*?)>\\[(.*?)\\]/?-?/?/?\\[?(.*?)\\]?\\s+";
	private static final Pattern stockP = Pattern.compile(stockPat, Pattern.DOTALL);

	private static final String txnPat = "\\s?([CMUYDPX]?)([0-9,.-]+)\\s?";
	private static final Pattern txnP = Pattern.compile(txnPat, Pattern.DOTALL);

	private static final String INPUT =
"\n" + 
"CODE  NAME OF STOCK    SALES RECORD\n" + 
"    1 CHEUNG KONG      < P52,000-122.627 P52,000-122.626 P7,000-122.639\n" + 
"                       110,000-122.30 >[ 18,000-122.20 2,000-122.00 \n"+ 
"                       1,000-121.30 1,000-121.40 3,000-121.30 10,000-121.40 \n"+ 
"                       13,000-121.50 2,000-121.60 ]/-//[ 3,000-121.60 \n"+ 
"                       3,000-120.70 4,000-120.40 3,000-120.70 ]\n" + 
"    2 CLP HOLDINGS     < P14,500-62.626 P500-62.75 P12,500-62.55 500-62.25\n" +  
"                       U2,000-62.25 95,000-62.25 >[\n" + 
"                       4,000-61.80 500-61.75 1,000-61.85 1,500-61.80 500-61.85\n" +  
"                       ]/-//\n";

	public static void parseTransaction(String session) {
		System.out.println("PARSING: " + session);
		Matcher matcher = txnP.matcher(session);
		int start = 0;
		while (matcher.find(start)) {
			System.out.print("TXN: ");
			System.out.print(matcher.group(1));
			System.out.println(matcher.group(2));
			start = matcher.end();
		}
	}

	public static void main(String[] argv) {
		Matcher matcher = stockP.matcher(INPUT);
		int start = 0;
		while (matcher.find(start)) {
			System.out.println(matcher.group(1));
			System.out.println(matcher.group(2));
			parseTransaction(matcher.group(3));
			parseTransaction(matcher.group(4));
			parseTransaction(matcher.group(5));
			start = matcher.end();
		}
	}
}
